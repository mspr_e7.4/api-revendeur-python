from fastapi import APIRouter,HTTPException,Depends
from models.product import Product
from config.config import products as product_table
from schema.product import productEntity,productsEntity
import requests
from config.jwt_bearer import JWTBearer


product = APIRouter()

#afficher la liste des produits 
@product.get('/products',dependencies=[Depends(JWTBearer())])
async def show_all_products(page : int=1,limit:int=10):
    offset=(page-1)*limit
    return productsEntity(product_table.find().skip(offset).limit(limit))


#Selectionner un produit spécifique avec son identifiant
@product.get('/products/{id}',dependencies=[Depends(JWTBearer())])
async def search_product(id : int):
    try:
        return productEntity(product_table.find_one({"id" : id}))
    except:
        raise HTTPException(status_code=400,detail={"message" : "Product not found"})

#importer les produits
@product.post('/productimp',dependencies=[Depends(JWTBearer())])
async def import_products():

    # Récupérer les données à partir de l'API Mock
    response = requests.get("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/")
    products_data = response.json()
    product_table.drop()
    
    # inserer les données dans la collection produits à partir des données récupérées
    products = []
    for product_data in products_data:
        product = Product(**product_data)   
        product_table.insert_one(dict(product))    
    return productsEntity(product_table.find())

