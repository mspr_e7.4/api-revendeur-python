from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
from dotenv import load_dotenv
import smtplib
from fastapi import APIRouter,HTTPException
from models.user import UserInputMFA,MFAAuthentificationResponse,Userdb,UserInput
from config.config import users as user_table
from passlib.context import CryptContext
from config.jwt_handler import sign_jwt
import pyotp
import qrcode
load_dotenv()
user = APIRouter()

#Variable used to crypt the password
pw_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

#def check password not empty
def check_password(password:str):
    if password == "" :
        raise HTTPException(status_code=400,detail={"message":"Username or password wrong!"})
#def check user exists
def check_user(u:UserInput)-> Userdb:
    check_password(u.password)
    #verify if user exists in database and if password is correct
    user_in_db=user_table.find_one({"email" : u.email})
    if not user_in_db:
        raise HTTPException(status_code=404,detail={"message":"Username or password wrong!"})
    if not pw_context.verify(u.password,user_in_db["hashed_pass"]):
        raise HTTPException(status_code=404,detail={"message":"Username or password wrong!"})
    return Userdb(**user_in_db)
    
##########################################################################################################################
#Create User
@user.post('/sign_up')
async def  sign_up(u : UserInput):
    
    check_password(u.password)
    #get users from db
    user_in_db=user_table.find_one({"email" : u.email})
    #verify if user exists in database
    if user_in_db :
        raise HTTPException(status_code=400, detail={"message":"this email is already used!"})
    #hashing password before insering into database
    hashed_pass=pw_context.hash(u.password)
    # Generate a secret key
    totp_secret = pyotp.random_base32()

    user_db = Userdb(**dict(u),hashed_pass=hashed_pass,totp_secret=totp_secret)
    #insert in database
    user_table.insert_one(dict(user_db))
    # Generate the QR code
    qr_code = pyotp.totp.TOTP(totp_secret).provisioning_uri(name=u.email, issuer_name="PayTonKawa")
    qr = qrcode.QRCode(version=1, box_size=10, border=5)
    qr.add_data(qr_code)
    qr.make(fit=True)
    # Create an image from the QR code and save it to a file
    qr_img = qr.make_image(fill_color="black", back_color="white")
    qr_img.save("qr_code.png")
    # Set up email
    msg = MIMEMultipart()
    msg["From"] = "hatimxben@gmail.com"
    msg["To"] = u.email
    msg["Subject"] = "QR code image"
    body = MIMEText("Please find attached the QR code image.")
    msg.attach(body)
    # Attach image
    with open("qr_code.png", "rb") as f:
        img_data = f.read()
        image = MIMEImage(img_data, name=os.path.basename("qr_code.png"))
        msg.attach(image)
    # Send email
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    server.login(os.getenv('sender_email'), os.getenv('sender_password'))
    server.sendmail(os.getenv('sender_email'), u.email, msg.as_string())
    server.quit()

    # Delete image file
    os.remove("qr_code.png")
    return {"message": "User create check mail for qr_code to scan with google authenticator"}

#verify user
@user.post('/sign_in',response_model=MFAAuthentificationResponse)
async def sign_in(u:UserInputMFA):
    user_in_db=check_user(u)
    # Create an instance of the TOTP class
    totp = pyotp.TOTP(user_in_db.totp_secret)
    # Verify MFA token
    
    if totp.verify(u.mfa_token):
        # Generate and return JWT token
        jwt_token = sign_jwt(u.email)

        return {"token": jwt_token}
    else:
        raise HTTPException(status_code=401, detail="Invalid MFA token")
