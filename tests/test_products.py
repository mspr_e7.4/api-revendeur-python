from fastapi.testclient import TestClient
from main import app
import unittest
from config.jwt_handler import sign_jwt
import coverage
cov = coverage.Coverage()
cov.start()


client = TestClient(app)


class TestProducts(unittest.TestCase):

#unit tests on products
    def test_get_products_from_mock_api(self):
            jwt_token = sign_jwt("hatimxben@gmail.com")
            headers = {"Authorization": f"Bearer {jwt_token}"}
            response = client.post("/productimp", headers=headers)
            assert response.status_code == 200
            assert "_id" in response.text
            assert "createdAt" in response.text
            assert "name" in response.text
            assert "details" in response.text
            assert "price" in response.text
            assert "description" in response.text
            assert "color" in response.text
            assert "stock" in response.text
            assert "id" in response.text

    def test_get_products_with_valid_token(self):
        jwt_token = sign_jwt("hatimxben@gmail.com")
        headers = {"Authorization": f"Bearer {jwt_token}"}
        response = client.get("/products", headers=headers)
        assert response.status_code == 200
        assert "_id" in response.text
        assert "createdAt" in response.text
        assert "name" in response.text
        assert "details" in response.text
        assert "price" in response.text
        assert "description" in response.text
        assert "color" in response.text
        assert "stock" in response.text
        assert "id" in response.text
    
    def test_get_product_with_invalid_token(self):
        jwt_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJleHAiOjE2NzkxxzkyMTYuODAyNjYyNH0.Mc2yL5LQBb4Aepuw0QRPM9nJpUSir6H6BEDnDbieDsA"
        headers = {"Authorization": f"Bearer {jwt_token}"}
        response = client.get("/products?page=1&limit=10", headers=headers)
        response_json = response.json()
        assert response.status_code == 403
        assert response_json == {"detail":"Invalid token or expired token."}

    def test_get_one_product_with_invalid_token(self):
        jwt_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJleHAiOjE2NzkxxzkyMTYuODAyNjYyNH0.Mc2yL5LQBb4Aepuw0QRPM9nJpUSir6H6BEDnDbieDsA"
        headers = {"Authorization": f"Bearer {jwt_token}"}
        response = client.get("/products/{1}", headers=headers)
        response_json = response.json()
        assert response.status_code == 403
        assert response_json == {"detail":"Invalid token or expired token."}
    
    def test_get_one_product_with_invalid_id_and_valid_token(self):
        jwt_token = sign_jwt("hatimxben@gmail.com")        
        headers = {"Authorization": f"Bearer {jwt_token}"}
        response = client.get("/products/500", headers=headers)
        response_json = response.json()
        assert response.status_code == 400
        assert response_json == {"detail":{"message":"Product not found"}}
    
    def test_get_one_product_with_valid_id_valid_token(self):
        jwt_token = sign_jwt("hatimxben@gmail.com")        
        headers = {"Authorization": f"Bearer {jwt_token}"}
        response = client.get("/products/3", headers=headers)
        response_json = response.json()
        response_json.pop("_id")
        assert response.status_code == 200
        assert response_json == {'createdAt': '2023-02-20T09:57:59.008Z', 'name': 'Café de Julien Couraud', 'details': {'price': '80.00', 'description': 'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J', 'color': 'indigo'}, 'stock': 0, 'id': 3}

cov.stop()
cov.save()
