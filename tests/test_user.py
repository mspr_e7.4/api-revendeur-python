from fastapi.testclient import TestClient
from main import app
import unittest
from config.config import users
import coverage
cov = coverage.Coverage()
cov.start()


client = TestClient(app)


class TestUser(unittest.TestCase):

#unit tests on sign up

    def test_sign_up_new_user(self):
        data = {"email": "hatim@example.com","password": "string"}
        users.delete_one({'email': "hatim@example.com"})
        response = client.post("/sign_up", json=data)
        reponse_json = response.json()
        assert response.status_code == 200
        assert reponse_json == {"message": "User create check mail for qr_code to scan with google authenticator"}

        
    def test_sign_up_user_exists(self):
        data = {"email": "hatim@example.com","password": "string"}
        response = client.post("/sign_up", json=data)
        reponse_json = response.json()
        assert response.status_code == 400
        assert reponse_json == {'detail': {'message': 'this email is already used!'}}

    def test_sign_up_password_not_empty(self):
        data = {"email": "hatim@example.com","password": "",}
        response = client.post("/sign_up", json=data)
        reponse_json = response.json()
        assert response.status_code == 400
        assert reponse_json == {"detail": {"message": "Username or password wrong!"}}
            




#unit tests on sign in
    def test_sign_in_password_not_empty(self):
        data = {"email": "test@example.com","password": "","mfa_token":""}
        response = client.post("/sign_in", json=data)
        reponse_json = response.json()
        assert response.status_code == 400
        assert reponse_json == {"detail": {"message": "Username or password wrong!"}}

    def test_sign_in_email_wrong(self):
        data = {"email": "test@example.com","password": "aaa","mfa_token":""}
        response = client.post("/sign_in", json=data)
        reponse_json = response.json()
        assert response.status_code == 404
        assert reponse_json == {"detail": {"message": "Username or password wrong!"}}

    def test_sign_in_password_wong(self):
        data = {"email": "hatim@example.com","password": "aaa","mfa_token":""}
        response = client.post("/sign_in", json=data)
        reponse_json = response.json()
        assert response.status_code == 404
        assert reponse_json == {"detail": {"message": "Username or password wrong!"}}

    def test_sign_in_invalid_mfa_token(self):
        data = {"email": "hatim@example.com","password": "string","mfa_token":"502369"}
        response = client.post("/sign_in", json=data)
        reponse_json = response.json()
        assert response.status_code == 401
        assert reponse_json == {'detail': 'Invalid MFA token'}


        


cov.stop()
cov.save()
