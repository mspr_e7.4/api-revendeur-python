from pydantic import BaseModel,EmailStr

#models de class user

class User(BaseModel):
    email : EmailStr

class Userdb(User):
    hashed_pass : str
    totp_secret : str

class UserInput(User):
    password : str


class UserInputMFA(UserInput):
    mfa_token : str

class MFAAuthentificationResponse(BaseModel):
    token:str