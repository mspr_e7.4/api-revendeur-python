def productEntity(item) -> dict:
    return {
        "_id":str(item["_id"]),
        "createdAt":str(item["createdAt"]),
        "name":item["name"],
        "details":item["details"],
        "stock":item["stock"],
        "id":item["id"]
    }

def productsEntity(entity) -> list:
    return [productEntity(item) for item in entity]