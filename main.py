from fastapi import FastAPI
from routes.product import product
from routes.user import user
from dotenv import load_dotenv
load_dotenv()

app = FastAPI()
app.include_router(product)
app.include_router(user)


@app.get("/")
async def health_check():
    return {"message":"OK"}