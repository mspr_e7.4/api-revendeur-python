#This file is responsible for signing,decoding,encoding JWT

import time
import jwt
import os
from dotenv import load_dotenv
load_dotenv()

JSK=os.getenv('SK')
ALG=os.getenv('alg')

def sign_jwt(email : str):
    payload = {
        "email" : email,
        "exp" : time.time()+3600
    }
    token=jwt.encode(payload,JSK,ALG)
    return(token)

def decode_jwt(token:str):
    decode_token=jwt.decode(token,JSK,ALG)
    return(decode_token)