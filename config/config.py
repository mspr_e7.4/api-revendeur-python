from pymongo import MongoClient
import os
from dotenv import load_dotenv
load_dotenv()

# Connexion à la base de données MongoDB

db_url = os.getenv('DATABASE_URL')
client = MongoClient(db_url)
users= client.db.users
products=client.db.products